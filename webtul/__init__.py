#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Webtul is short for WEB toolkit
See modules of this package for more details.
"""
__author__ = 'Zagfai'
__license__ = 'MIT@2014-01'

import log
import db
import utils

__all__ = ['log', 'db', 'utils', 'task', 'struct']
__version__ = '0.37'

#from os.path import dirname, abspath, join
#folder = lambda x='': join(dirname(abspath(__file__)), x)

